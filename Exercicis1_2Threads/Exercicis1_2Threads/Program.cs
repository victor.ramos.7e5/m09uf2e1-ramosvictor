﻿using System;
using System.IO;
using System.Linq;
using System.Threading;


namespace Exercicis1_2Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            Exercici1();
        }

        


        static void Exercici1()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Aquesta linia està en el progrés principal");

            /***** Pas de paràmetre en un thread***/
            Thread myFirstThread = new Thread(MyThreadFunc);
            myFirstThread.Start("Leectura.txt");
            

            Console.ForegroundColor = ConsoleColor.Green;
            Thread secondThread = new Thread(MyThreadFunc);
            secondThread.Start("Leectura2.txt");
            Console.ForegroundColor = ConsoleColor.Blue;
            Thread thirstThread = new Thread(MyThreadFunc);
            thirstThread.Start("Leectura3.txt");
        }
        static void MyThreadFunc(object o)
        {
            string s = Convert.ToString(o);
            var file = File.ReadLines(@"../../../../"+s).Count();
            Console.WriteLine("Tiene " + file + " lineas");
        }


    }
}
